package ru.otus.microservices.health.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import ru.otus.microservices.health.enums.HealthStatus;

@AllArgsConstructor
public class HealthInfoDto {

    @JsonProperty("status")
    private HealthStatus status;

}
